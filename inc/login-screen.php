<?php

namespace NM\White\LoginScreen;

add_filter( 'login_headerurl', __NAMESPACE__ . '\\edit_link' );
add_filter( 'login_headertitle', '__return_false' );
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\replace_logo' );
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\modify_styles' );



function edit_link() {
	return 'https://035media.se/';
}

function replace_logo() {
	echo '<style type="text/css">';
	echo "
		body.login div#login h1 a {
			background-image: url(" . NM_WHITE_URL . "img/logo.png);
		}
	";
	echo '</style>';
}

function modify_styles() {
	$colors = [
		'primary'  => '#fc7658',
		'dark'     => '#404040',
		'mid'      => '#7f7f7f',
		'light'    => '#bfbfbf',
		'lighter'  => '#f1f1f1',
		'lightest' => '#fafafa',
	];

	echo '<meta name="theme-color" content="' . $colors['lighter'] . '">';

	echo '<style type="text/css">';
	echo "
		*, *::before, *::after {
			box-sizing: border-box;
		}

		body.login {
			background-color: {$colors['lighter']};

			color: {$colors['dark']};
			font-size: 16px;
			line-height: 1.5;
		}

		body.login div#login {
			padding-top: 12%;
		}

		body.login div#login h1 {
			margin-bottom: 24px;

			pointer-events: none;
		}

		body.login div#login h1 a {
			height: 128px;
			margin: 0 auto;
			width: 128px;

			background-position: center center;
			background-size: 128px;

			color: transparent;
			font-size: 0;
			line-height: 0;
			text-indent: initial;

			pointer-events: none;
		}

		body.login div#login form#loginform,
		body.login div#login form#lostpasswordform,
		body.login div#login form#registerform {
			margin: 24px 0 0;
			padding: 16px;

			border-radius: 2px;

			box-shadow: none;
			box-shadow: 0 1px 1px rgba(0, 0, 0, .1);
		}

		body.login div#login form#loginform p,
		body.login div#login form#lostpasswordform p,
		body.login div#login form#registerform p {
			margin-bottom: 16px;
		}

		body.login div#login form#loginform p label,
		body.login div#login form#lostpasswordform p label,
		body.login div#login form#registerform p label {
			display: block;
			position: relative;

			color: {$colors['mid']};
			font-size: 12px;
			line-height: 24px;
		}

		body.login div#login form#loginform p label::before,
		body.login div#login form#lostpasswordform p label::before,
		body.login div#login form#registerform p label::before {
			content: '';
			display: block;
			height: 40px;
			position: absolute;
			bottom: 1px;
			left: 1px;
			width: 40px;

			border-top-left-radius: 1px;
			border-bottom-left-radius: 1px;

			color: {$colors['light']};
			font-family: dashicons;
			font-size: 32px;
			line-height: 40px;
			text-align: center;

			cursor: pointer;
		}

		body.login div#login form#loginform p:first-child label::before,
		body.login div#login form#lostpasswordform p:first-child label::before,
		body.login div#login form#registerform p:first-child label::before {
			content: \"\\f110\";
		}

		body.login div#login form#loginform p:nth-child(2) label::before {
			content: \"\\f160\";
		}

		body.login div#login form#registerform p:nth-child(2) label::before {
			content: \"\\f466\";
		}

		body.login div#login form#loginform input,
		body.login div#login form#lostpasswordform input,
		body.login div#login form#registerform input {
			height: auto;
			margin: 0;
			padding: 4px;
			padding-left: 44px;

			border: 1px solid {$colors['lighter']};
			border-radius: 2px;

			background-color: {$colors['lightest']};

			color: {$colors['dark']};
			font-size: 24px;
			line-height: 32px;

			box-shadow: none;
			transition: border-color 150ms ease-out;
		}
		body.login div#login form#loginform input:focus,
		body.login div#login form#lostpasswordform input:focus,
		body.login div#login form#registerform input:focus {
			border-color: {$colors['light']};
		}

		body.login div#login form#registerform p#reg_passmail {
			margin-bottom: 0;

			font-size: 12px;
			line-height: 16px;
		}

		body.login div#login form#loginform p.forgetmenot {
			margin: 0;
			padding: 0;
			width: calc(50% - 8px);
		}

		body.login div#login form#loginform p.forgetmenot label {
			line-height: 42px;
		}

		body.login div#login form#loginform p.forgetmenot label::before {
			content: initial;
			display: none;
		}

		body.login div#login form#loginform p.forgetmenot input#rememberme {
			height: 24px;
			margin-right: 4px;
			width: 24px;
		}

		body.login div#login form#loginform p.forgetmenot input#rememberme:before {
			margin: -14px -12px !important;

			color: {$colors['primary']} !important;
			font-size: 48px !important;
		}

		body.login div#login form#loginform p.submit,
		body.login div#login form#lostpasswordform p.submit,
		body.login div#login form#registerform p.submit {
			float: right;
			margin-bottom: 0;
			width: calc(50% - 8px);
		}

		body.login div#login form#loginform p.submit input#wp-submit,
		body.login div#login form#lostpasswordform p.submit input#wp-submit,
		body.login div#login form#registerform p.submit input#wp-submit {
			padding: 4px;
			width: 100%;

			border-color: #e07259;

			background-color: {$colors['primary']};

			color: white;
			font-size: 16px;
			text-align: center;
			text-shadow: none;
		}
		body.login div#login form#lostpasswordform p.submit input#wp-submit,
		body.login div#login form#registerform p.submit input#wp-submit  {
			padding: 4px 16px;
			width: auto;
		}

		body.login div#login p#nav {
			padding: 0 16px;

			color: {$colors['light']};
			font-size: 12px;
			text-align: left;
		}

		body.login div#login p#backtoblog {
			margin: 24px 16px;
			padding: 24px 0;

			border-top: 1px dotted {$colors['light']};

			font-size: 12px;
			text-align: center;
		}

		body.login div#login p#nav a,
		body.login div#login p#backtoblog a {
			color: {$colors['mid']};
		}
		body.login div#login p#nav a:hover,
		body.login div#login p#backtoblog a:hover {
			color: {$colors['dark']};
		}


		body.login #login_error,
		body.login .message {
			padding: 16px 16px 16px 12px;

			border-radius: 2px;

			font-size: 12px;
			line-height: 16px;

			box-shadow: 0 1px 1px rgba(0, 0, 0, .1);
		}
	";
	echo '</style>';
}

<?php

namespace NM\White\AdminFooter;

add_action( 'admin_footer_text', __NAMESPACE__ . '\\edit_text' );



function edit_text() {
	echo '<span id="footer-thankyou">';
		echo 'Utvecklad med ';
		echo '<a href="https://wordpress.org/" target="_blank">WordPress</a>';
		echo ' av ';
		echo '<a href="https://035media.se/" target="_blank">035 Media Group</a>';
	echo '.</span>';
}

<?php

namespace NM\White\AdminBar;

add_action( 'admin_bar_menu', __NAMESPACE__ . '\\remove_wp_logo', 999 );
add_action( 'admin_bar_menu', __NAMESPACE__ . '\\remove_my_account', 999 );
add_action( 'admin_bar_menu', __NAMESPACE__ . '\\add_logout_link', -999 );



// Remove WP logo from admin bar.
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}

// Remove "My Account" from admin bar.
function remove_my_account( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'my-account' );
}

// Add our custom logout link.
function add_logout_link( $wp_admin_bar ) {
	$wp_admin_bar->add_node( [
		'id' => 'nm-logout',
		'title' => 'Logga ut',
		'parent' => 'top-secondary',
		'href' => wp_logout_url(),
	] );
}

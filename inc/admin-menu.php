<?php

namespace NM\White\AdminMenu;

add_action( 'admin_menu', __NAMESPACE__ . '\\remove_items' );



// Remove "Tools" for everyone, except admins.
function remove_items() {
	if ( ! current_user_can( 'manage_options' ) ) {
		remove_menu_page( 'tools.php' );
	}
}

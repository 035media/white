<?php

namespace NM\White\Dashboard;

add_action( 'admin_init', __NAMESPACE__ . '\\remove_widgets' );



function remove_widgets() {
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
}

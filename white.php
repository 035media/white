<?php
/*
Plugin Name: White
Description: Whitelabel-tillägg för hemsidor producerade av 035 Media Group.
Version: 7.0.2
Author: 035 Media Group
Author URI: https://035media.se
License: MIT
GitLab Plugin URI: 035media/white
*/

namespace ZTFM;

defined('WPINC') || die();

class White
{
    protected static $instance;

    /**
     * Boot up the plugin.
     */
    protected function __construct()
    {
        add_action('admin_bar_menu', [$this, 'removeAdminBarItems'], 999);
        add_action('admin_print_styles', [$this, 'updateAdminFontStack'], 20);
        add_action('admin_footer_text', [$this, 'editAdminFooterText']);

        add_action('wp_dashboard_setup', [$this, 'removeDashboardWidgets']);
        remove_action('welcome_panel', 'wp_welcome_panel');

        add_filter('login_headerurl', '__return_false');
        add_filter('login_headertext', '__return_empty_string');
        add_action('login_enqueue_scripts', [$this, 'editLoginStyles']);

        remove_action('wp_head', 'wp_generator');
    }

    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Remove the WordPress logo from the admin bar.
     *
     * @param \WP_Admin_Bar $wpAdminBar
     * @return void
     */
    public function removeAdminBarItems($wpAdminBar)
    {
        $wpAdminBar->remove_node('wp-logo');
    }

    /**
     * Add emoji support to the default WordPress font stack.
     *
     * @return void
     */
    public function updateAdminFontStack()
    {
        echo '<style>body{font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"}</style>';
    }

    /**
     * Edit the footer notice on admin screens.
     *
     * @return string
     */
    public function editAdminFooterText()
    {
        return 'Utvecklad med ❤ och ☕ av <a href="https://035media.se" target="_blank" rel="noopener">035 Media Group</a>.';
    }

    /**
     * Remove dashboard widgets.
     *
     * @return void
     */
    public function removeDashboardWidgets()
    {
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
        remove_meta_box('dashboard_primary', 'dashboard', 'side');
    }

    /**
     * Show a custom logo on the login screen, if available in the current theme.
     * If not, just hide the default WordPress logo.
     *
     * @return void
     */
    public function editLoginStyles()
    {
        if ($this->currentThemeHasLoginLogo()) {
            echo $this->loginLogoStyles();
        } else {
            echo '<style>.login h1{display:none !important}</style>';
        }
    }

    /**
     * Generate CSS for the login screen.
     *
     * @return string
     */
    protected function loginLogoStyles()
    {
        $css = '.login h1 a {
            background-image: url({{url}}) !important;
            background-position: center bottom !important;
            background-size: contain !important;
            height: 6rem !important;
            margin: 0 auto !important;
            padding-bottom: 0 !important;
            pointer-events: none !important;
            width: 100% !important;
        }';

        return '<style>' .
            '.login h1 { padding: 0 24px !important }' .
            str_replace('{{url}}', $this->currentThemeLoginLogo(), $css) .
            '</style>';
    }

    /**
     * Get the login logo url from the current theme.
     *
     * @return string|void
     */
    protected function currentThemeLoginLogo()
    {
        if ($this->currentThemeHasFile('login-logo.svg')) {
            return get_stylesheet_directory_uri() . '/login-logo.svg';
        }

        if ($this->currentThemeHasFile('login-logo.png')) {
            return get_stylesheet_directory_uri() . '/login-logo.png';
        }
    }

    /**
     * Check if the current theme has a custom login logo.
     *
     * @return bool
     */
    protected function currentThemeHasLoginLogo()
    {
        return $this->currentThemeHasFile('login-logo.svg') ||
            $this->currentThemeHasFile('login-logo.png');
    }

    /**
     * Check if the current theme has a specified file.
     *
     * @param string $file
     * @return bool
     */
    protected function currentThemeHasFile($file)
    {
        return file_exists(get_stylesheet_directory() . "/$file");
    }
}

White::instance();

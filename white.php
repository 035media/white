<?php
/*
Plugin Name: White
Description: Döljer WordPress logotyper i adminpanelen och anpassar stilen på inloggningsformuläret.
Version:     2.0.2
Author:      035 Media Group
Author URI:  https://035media.se/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Bitbucket Plugin URI: 035media/white
*/

namespace NM\White;

defined( 'WPINC' ) || die;

define( 'NM_WHITE_URL', plugin_dir_url( __FILE__ ) );

require_once 'inc/admin-bar.php';
require_once 'inc/admin-menu.php';
require_once 'inc/admin-footer.php';
require_once 'inc/dashboard.php';
require_once 'inc/login-screen.php';
